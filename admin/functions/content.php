<?php

function getAllContent() {
  return queryDatabase("SELECT * FROM content ORDER BY id ASC");
}

function createContent($slug, $options) {
  return queryDatabase("INSERT INTO content (slug, options) VALUES (?, ?)", array($slug, $options));
}

function getContent($slug, $option_key) {
  $options = queryDatabase("SELECT options FROM content WHERE slug = ?", array($slug));

  return json_decode($options[0]['options'], true)[$option_key];
}

function updateContent($id, $slug, $options) {
  return queryDatabase("UPDATE content SET slug = ?, options = ? WHERE id = ?", array($slug, $options, $id));
}

function deleteContent($id) {
  return queryDatabase("DELETE FROM content WHERE id = ?", array($id));
}

?>