<?php

include('mock_data.php');

function createMockDatabase() {
  global $MOCK_DATA;
  global $MYSQL;

  $mysql_host = $MYSQL['host'];
  $mysql_db = $MYSQL['db'];
  $mysql_user = $MYSQL['user'];
  $mysql_pass = $MYSQL['pass'];

  $dbh = new PDO("mysql:host=$mysql_host", $mysql_user, $mysql_pass);

  $dbh->exec("CREATE DATABASE `$mysql_db`;
    GRANT ALL ON `$mysql_db`.* TO '$mysql_user'@'$mysql_host';
    FLUSH PRIVILEGES;");

  $dbh = null;
  $dbh = new PDO('mysql:host=' . $mysql_host . ';dbname=' . $mysql_db, $mysql_user, $mysql_pass);

  $dbh->exec("CREATE TABLE IF NOT EXISTS `content` (`id` INT AUTO_INCREMENT NOT NULL, `slug` varchar(255), `options` TEXT, `order` INT(3), PRIMARY KEY (`id`))");
  $dbh->exec("CREATE TABLE IF NOT EXISTS `applications` (`id` INT AUTO_INCREMENT NOT NULL, `options` text, `status` varchar(255), `date_submitted` varchar(255), `ip` varchar(25), PRIMARY KEY (`id`))");
  $dbh->exec("CREATE TABLE IF NOT EXISTS `users` (`id` INT AUTO_INCREMENT NOT NULL, `email` varchar(255), `password` TEXT, PRIMARY KEY (`id`))");

  foreach ($MOCK_DATA['content'] as $content) {
    $sth = $dbh->prepare("INSERT INTO content (slug, options) VALUES (?, ?)");
    $sth->execute(array($content['slug'], $content['options']));
  }

  foreach ($MOCK_DATA['applications'] as $application) {
    $sth = $dbh->prepare("INSERT INTO applications (options, status, date_submitted, ip) VALUES (?, ?, ?, ?)");
    $sth->execute(array($application['options'], $application['status'], $application['date'], $application['ip']));
  }

  foreach ($MOCK_DATA['users'] as $user) {
    $sth = $dbh->prepare("INSERT INTO users (email) VALUES (?)");
    $sth->execute(array($user['email']));
  }
  
  $dbh = null;

  echo "Database `" . $mysql_db . "` did not exist. It has been created and updated with filler content. Refresh to dismiss this message.";
}

$e->getCode() == 1049 ? createMockDatabase() : print "Error!: " . $e->getMessage();
// error code 1049 is missing database

?>