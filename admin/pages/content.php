<button class="small button add-content">Add Content</button>
<hr>

<?php foreach (getAllContent() as $content) {?>

<div class="content" data-id="<?php echo $content['id']; ?>">

  <input type="text" class="name" placeholder="slug" value="<?php echo $content['slug']; ?>"/>
        
<?php foreach (json_decode($content['options']) as $option_name => $option_value) { ?>

  <div class="row key-value-pairs">
    <div class="large-5 columns">
      <input type="text" class="key" placeholder="Key" value="<?php echo $option_name; ?>"/>
    </div>

    <div class="large-5 columns">
      <input type="text" class="value" placeholder="Value" value="<?php echo $option_value; ?>"/>
    </div>

    <div class="large-2 columns">
      <button class="tiny alert delete button right">Delete</button>
    </div>
  </div>

<?php } // end $options foreach ?>
        
  <button class="tiny button add">Add Content Key - Value Pair</button>

  <br>

  <button class="small button update-content">Update</button>
  <button class="small alert button delete-content">Delete</button>

  <hr>
</div>

<?php } // end $content foreach ?>

<h3>Usage</h3>
<code><?php echo htmlentities('getContent([content_slug], [content_option])'); ?></code>
<br><br>
<h4>Example</h4>

<?php $exampleContent = getAllContent()[0];
      $exampleContentSlug = $exampleContent['slug'];
      $exampleContentOption = json_decode($exampleContent['options'], true); ?>

<p>
  <strong>Code</strong>
  <br>
  <code><?php echo htmlentities('<?php echo getContent(\'' . $exampleContentSlug . '\', \'' . key($exampleContentOption) . '\'); ?>'); ?></code>
</p>

<p>
  <strong>Outputs</strong>
  <br>
  <?php echo getContent($exampleContentSlug, key($exampleContentOption)); ?>
</p>

<hr>