﻿
<?php include('functions.php'); ?>


<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <title>#HealSTL | Outreach</title>
    
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/app.css" />
    <link href='http://fonts.googleapis.com/css?family=Paytone+One|Muli:300,400|Open+Sans:300' rel='stylesheet' type='text/css'>
     
    <script src="js/vendor/modernizr.js"></script>
    <script>
        (function (p, s, h) {
            (p[s] = p[s] || []).push(
                ["_setAccount", "543825c4572b94d733283030"],
                ["_setCDN", h],
                ["_setUrl", "https://assets.pushup.com"],
                ["_displayBar"]
            );

            s = (p = p.document).createElement("script");
            s.src = h + "/pushup.min.js";
            p.getElementsByTagName("head")[0].appendChild(s)
        })(window, "_pa", "https://cdn.pushup.com");
    </script>
  </head>
  <body>
    <div class="contain-to-grid sticky">	
    	<nav class="top-bar" data-topbar>
      <ul class="title-area">
        <li class="name">	
          	<div class="row">
          		<div class="large-12 medium-12 small-6 small-centered columns">
          			<h1 class="logo"><a href="index.php"><img class="size" src="img/logo.png" alt="#healSTL" /></a></h1>
          		</div>	
          	</div>
        </li>
         <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
        <li class="toggle-topbar menu-icon"><a href="#"><span></span></a></li>
      </ul>
    
      <section class="top-bar-section">
        <!-- Right Nav Section -->
        <ul class="right">
	         <li><a href="volunteer.php">Volunteer</a></li>
	         <li class="active"><a href="outreach.php">Outreach</a></li>
	         <li><a href="about.php">About</a></li>
	         <li><a href="https://squareup.com/market/healstl/healstl-t-shirt">T-Shirts</a></li>
	         <li><a href="donate.php" class="button tiny" >Donate</a></li>
        </ul>
    
      </section>
    </nav>
    </div>
    	
	<div class="parallex outreach">
		<div class="row">
			<div class="large-12 medium-12 small-12 columns">
				<h1><?php getContent('outreach', 'title'); ?></h1>
			</div>
		</div>
	</div>
	
	<div class="action">
	    <div class="row">
	    	<div class="large-12 medium-12 small-12 columns">
	        	<h2><?php getContent('outreach_info', 'title'); ?></h2>  
	      	</div>    
	    </div>
	    
	    <div class="row">
	    	<div class="large-12 columns">
	        	<p class="hero"><?php getContent('outreach_info', 'text'); ?></p>
	     	</div>
	    </div>
	    <div class="row">
	    	<div class="large-6 medium-6 small-6 columns columns text-center">
	        	<a target="_blank" href="https://www.sos.mo.gov/votemissouri/request.php" class="button small">Online Registration</a>
	      	</div>
	    	<div class="large-6 medium-6 small-6 columns columns text-center">
	        	<a target="_blank" href="https://www.sos.mo.gov/elections/register2vote/St.Louis.pdf" class="button small">Download Registration</a>
	     	</div>
	    </div>
 	</div>
 	
    <!--<div class="row">
      <div class="large-12 columns text-center">
        <h2>Upcoming Events</h2>
      </div>
    </div>
    <div class="row">
    	<div class="small-12 medium-12 large-3 columns">
    		<h3>September 14th</h3>
    	</div>
    	<div class="small-12 medium-3 large-3 columns">
    		<img src="http://placehold.it/500x400/999999/ffffff&Insert+Image">
    	</div>
    	<div class="small-12 medium-9 large-6 columns">
    		<p class="title">Peace Rally</p>	
    		<p class="attention">
    			A quae elit et litteris, eram proident hic fabulas, ut nulla firmissimum, 
    			senserit ea cillum senserit. Noster non offendit ita quem non quibusdam nam 
    			elit, quamquam eu quibusdam, do ut nulla occaecat, do fugiat ita quid ea an 
    			admodum do deserunt sed laborum duis quis commodo aute hic do.
    		</p>   
    	</div>
    </div>
    <div class="row">
    	<div class="large-12 columns">
    		<hr />
    	</div>
    </div>
    <div class="row">
    	<div class="small-12 medium-12 large-3 columns">
    		<h3>September 25th</h3>  
    	</div>
    	<div class="small-12 medium-3 large-3 columns">
    		<img src="http://placehold.it/500x400/999999/ffffff&Insert+Image">
    	</div>
    	<div class="small-12 medium-9 large-6 columns">

    		<p class="title">Town-Hall Meeting</p>
    		<p class="attention">
    			A quae elit et litteris, eram proident hic fabulas, ut nulla firmissimum, 
    			senserit ea cillum senserit. Noster non offendit ita quem non quibusdam nam 
    			elit, quamquam eu quibusdam, do ut nulla occaecat, do fugiat ita quid ea an 
    			admodum do deserunt sed laborum duis quis commodo aute hic do. 
    		</p>
    	</div>
    </div>
  
    <div class="row">
    	<div class="large-12 columns">
    		<hr />
    	</div>
    </div>
      
    <div class="row">
    	<div class="small-12 medium-12 large-3 columns">
    		<h3>September 31st</h3>
    	</div>
    	<div class="small-12 medium-3 large-3 columns">
    		<img src="http://placehold.it/500x400/999999/ffffff&Insert+Image">
    	</div>
    	<div class="small-12 medium-9 large-6 columns">
    		<p class="title">March Downtown</p>
    		<p class="attention">
    			A quae elit et litteris, eram proident hic fabulas, ut nulla firmissimum, 
    			senserit ea cillum senserit. Noster non offendit ita quem non quibusdam nam 
    			elit, quamquam eu quibusdam, do ut nulla occaecat, do fugiat ita quid ea an 
    			admodum do deserunt sed laborum duis quis commodo aute hic do.
    			esse. 
    		</p>
    	</div>
    </div>  -->
    <div class="footer">
    	<div class="row">
            <div class="large-12 columns">
                <p>Powered by <a target="_blank" href="http://www.codecause.org">CodeCause.org</a></p>
            </div>
    	</div>
    </div>
    	 
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script>
      $(document).foundation();
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-55321242-1', 'auto');
        ga('send', 'pageview');

    </script>
  </body>
</html>